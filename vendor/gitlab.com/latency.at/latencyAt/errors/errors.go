package errors

import "errors"

var (
	ErrUserNotFound       = errors.New("User not found")
	ErrUserEmailExists    = errors.New("User with given email address already exists")
	ErrEmailTokenNotFound = errors.New("Email token not found")
	ErrTokenNotFound      = errors.New("Token not found")
	ErrTokenExpired       = errors.New("Token expired")
	ErrEventNotFound      = errors.New("Event not found")

	ErrAuthorizationMissing = errors.New("Invalid or missing Authorization header")
	ErrAuthorizationInvalid = errors.New("Invalid Authorization header")

	ErrBalanceLow = errors.New("Balance too low")

	ErrPasswordShort          = errors.New("Password too short")
	ErrCredentialsInvalid     = errors.New("Email or password wrong")
	ErrCredentialsUserInvalid = errors.New("Invalid username or password")
	ErrAccountNotActivated    = errors.New("Account not yet activated. Please check your mail.")

	ErrSubCustomerNotFound = errors.New("No stripe customer found")
	ErrSubNotFound         = errors.New("No subscription found")

	ErrJWTInvalid              = errors.New("Invalid JWT.")
	ErrJWTInvalidSigningMethod = errors.New("Invalid signing method")
	ErrJWTInvalidClaim         = errors.New("Token claim not jwt.MapClaims")
	ErrJWTInvalidUID           = errors.New("user_id claim is not a float64")
	ErrJWTInvalidActivated     = errors.New("activated claim is not a bool")

	ErrVATInvalid = errors.New("Invalid VAT number")

	ErrAuthNotSet   = errors.New("AuthUsername or password not set")
	ErrAuthRequired = errors.New("Require basic auth")

	ErrCardNotFound        = errors.New("Credit card not found")
	ErrCardNoCountry       = errors.New("Credit card has no country set")
	ErrSubsNumberInvalid   = errors.New("Invalid number of subscriptions")
	ErrSubsNotFound        = errors.New("No subscription found")
	ErrCustomerNotFound    = errors.New("No stripe customer found")
	ErrSubsInvalidData     = errors.New("Invalid data structure")
	ErrCardDeleteSubActive = errors.New("Can't remove card while subscription active")

	ErrCreateWithID = errors.New("Can't create entity with existing ID")

	// Internal errors, never been shown to clients
	ErrInternal = errors.New("Internal error")
)

type Error struct {
	Err        error
	StatusCode int
}

func (e *Error) Error() string {
	return e.Err.Error()
}
