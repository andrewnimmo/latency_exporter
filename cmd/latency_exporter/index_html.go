package main

const indexHTML = `<html>
  <head>
    <title>Latency.at Exporter</title>
    <style>
      @font-face {
        font-family: 'NDKaputt';
        src: url(https://latency.at/fonts/NDKaputtOffice-Medium.eot);
        src: url(https://latency.at/fonts/NDKaputtOffice-Medium.eot?#iefix) format("embedded-opentype"), url(https://latency.at/fonts/NDKaputtOffice-Medium.woff2) format("woff2"), url(https://latency.at/fonts/NDKaputtOffice-Medium.woff) format("woff");
        font-weight: normal;
        font-style: normal;
      }
      html {
        font-family: "NDKaputt";
        font-size: 24px;
        line-height: 1.85;
        letter-spacing: 0.05em;
        background-color: #18232f;
	color: #e8ecff;
      }
      body {
        position: absolute;
        top: 20%;
        left: 50%;
        width: 75%;
        max-width: 700px;
        transform: translateX(-50%);
      }
      a {
        text-decoration: none;
        color: #4478f0;
      }
      a:hover {
        color: #4478f0;
      }
      a:active {
        color: #4478f0 !important;
      }
    </style>
  </head>
  <body>
    <h1>Latency.at Exporter</h1>
    <p><a href="/probe?target=latency.at&module=http_2xx">Probe latency.at for http_2xx</a></p>
    <p>Visit <a href="https://latency.at">Latency.at</a> for more details.</p>
  </body>
</html>`
